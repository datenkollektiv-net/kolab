<?php

return [
    'host' => env('SMTP_HOST', '172.18.0.5'),
    'port' => env('SMTP_PORT', 10465),
];
