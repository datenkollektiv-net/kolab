<?php

namespace App\Mail;

use App\Payment;
use App\Tenant;
use App\User;
use App\Utils;

class PaymentSuccess extends Mailable
{
    /** @var \App\Payment A payment operation */
    protected $payment;


    /**
     * Create a new message instance.
     *
     * @param \App\Payment $payment A payment operation
     * @param \App\User    $user    A wallet controller to whom the email is being sent
     *
     * @return void
     */
    public function __construct(Payment $payment, User $user)
    {
        $this->payment = $payment;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $appName = Tenant::getConfig($this->user->tenant_id, 'app.name');
        $supportUrl = Tenant::getConfig($this->user->tenant_id, 'app.support_url');

        $subject = \trans('mail.paymentsuccess-subject', ['site' => $appName]);

        $this->view('emails.html.payment_success')
            ->text('emails.plain.payment_success')
            ->subject($subject)
            ->with([
                    'site' => $appName,
                    'subject' => $subject,
                    'username' => $this->user->name(true),
                    'walletUrl' => Utils::serviceUrl('/wallet', $this->user->tenant_id),
                    'supportUrl' => Utils::serviceUrl($supportUrl, $this->user->tenant_id),
            ]);

        return $this;
    }

    /**
     * Render the mail template with fake data
     *
     * @param string $type Output format ('html' or 'text')
     *
     * @return string HTML or Plain Text output
     */
    public static function fakeRender(string $type = 'html'): string
    {
        $payment = new Payment();
        $user = new User([
              'email' => 'test@' . \config('app.domain'),
        ]);

        $mail = new self($payment, $user);

        return Helper::render($mail, $type);
    }
}
