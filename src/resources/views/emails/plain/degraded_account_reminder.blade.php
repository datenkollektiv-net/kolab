{!! __('mail.header', ['name' => $username]) !!}

{!! __('mail.degradedaccountreminder-body1', ['site' => $site]) !!}

{!! __('mail.degradedaccountreminder-body2', ['site' => $site]) !!}

{!! __('mail.degradedaccountreminder-body3', ['site' => $site]) !!}

{!! $dashboardUrl !!}

{!! __('mail.degradedaccountreminder-body4', ['site' => $site]) !!}

{!! __('mail.degradedaccountreminder-body5', ['site' => $site]) !!}

-- 
{!! __('mail.footer1') !!}
{!! __('mail.footer2', ['site' => $site]) !!}
