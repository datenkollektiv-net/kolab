{!! __('mail.signupinvitation-header') !!}

{!! __('mail.signupinvitation-body1', ['site' => $site]) !!}

{!! $href !!}

{!! __('mail.signupinvitation-body2') !!}

-- 
{!! __('mail.footer1') !!}
{!! __('mail.footer2', ['site' => $site]) !!}
