<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Invalid username or password.',
    'password' => 'The provided password is incorrect.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'logoutsuccess' => 'Successfully logged out.',

    'error.password' => "Invalid password",
    'error.geolocation' => "Country code mismatch",
    'error.nofound' => "User not found",
    'error.2fa' => "Second factor failure",
    'error.2fa-generic' => "Second factor failure",

];
